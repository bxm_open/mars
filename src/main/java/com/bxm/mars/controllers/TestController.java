package com.bxm.mars.controllers;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;

@EnableAutoConfiguration
@RefreshScope
@RestController
public class TestController {

    @Value("${foo}")
    String foo;

    @RequestMapping("/test")
    public String test() {
        return this.foo;
    }

    @RequestMapping("/info")
    public String info() {
        return "{}";
    }
}
